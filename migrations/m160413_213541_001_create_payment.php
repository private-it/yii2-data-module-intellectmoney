<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160413_213541_001_create_payment
 *
 */
class m160413_213541_001_create_payment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(\PrivateIT\modules\intellectmoney\models\Payment::tableName(), [
            'id' => $this->primaryKey(),
            'eshop_id' => $this->integer()->defaultValue(0),
            'payment_id' => $this->integer()->defaultValue(0),
            'order_id' => $this->integer()->defaultValue(0),
            'eshop_account' => $this->string()->defaultValue(""),
            'service_name' => $this->string()->defaultValue(""),
            'recipient_original_amount' => $this->money()->defaultValue(0),
            'recipient_amount' => $this->money()->defaultValue(0),
            'recipient_currency' => $this->string()->defaultValue(""),
            'payment_status' => $this->integer()->defaultValue(0),
            'user_name' => $this->string()->defaultValue(""),
            'user_email' => $this->string()->defaultValue(""),
            'payment_data' => $this->dateTime()->defaultValue(null),
            'pay_method' => $this->string()->defaultValue(""),
            'short_pan' => $this->string()->defaultValue(""),
            'country' => $this->string()->defaultValue(""),
            'bank' => $this->string()->defaultValue(""),
            'ip_address' => $this->string()->defaultValue(""),
            'secret_key' => $this->string()->defaultValue(""),
            'hash' => $this->string()->defaultValue(""),
            'params' => $this->text()->defaultValue(""),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(\PrivateIT\modules\intellectmoney\models\Payment::tableName());
    }
}