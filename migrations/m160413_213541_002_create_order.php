<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160413_213541_002_create_order
 *
 */
class m160413_213541_002_create_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(\PrivateIT\modules\intellectmoney\models\Order::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(0),
            'eshop_id' => $this->integer()->defaultValue(0),
            'service_name' => $this->text()->defaultValue(""),
            'sum' => $this->money()->defaultValue(0),
            'recipient_currency' => $this->string()->defaultValue(""),
            'user_name' => $this->string()->defaultValue(""),
            'user_email' => $this->string()->defaultValue(""),
            'success_url' => $this->string()->defaultValue(""),
            'preference' => $this->string()->defaultValue(""),
            'expire_date' => $this->dateTime()->defaultValue(null),
            'params' => $this->text()->defaultValue(""),
            'hash' => $this->string()->defaultValue(""),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(\PrivateIT\modules\intellectmoney\models\Order::tableName());
    }
}