<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\intellectmoney\models;

use PrivateIT\modules\intellectmoney\IntellectMoneyModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Order
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $eshop_id
 * @property string $service_name
 * @property string $sum
 * @property string $recipient_currency
 * @property string $user_name
 * @property string $user_email
 * @property string $success_url
 * @property string $preference
 * @property string $expire_date
 * @property string $params
 * @property string $hash
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Payment $payment
 */
class Order extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('intellect-money/order', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('intellect-money/order', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('intellect-money/order', 'const.status.active'),
        ];
    }

    /**
     * @inheritdoc
     * @return query\OrderActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return IntellectMoneyModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('intellect-money/order', 'label.id'),
            'user_id' => Yii::t('intellect-money/order', 'label.user_id'),
            'eshop_id' => Yii::t('intellect-money/order', 'label.eshop_id'),
            'service_name' => Yii::t('intellect-money/order', 'label.service_name'),
            'sum' => Yii::t('intellect-money/order', 'label.sum'),
            'recipient_currency' => Yii::t('intellect-money/order', 'label.recipient_currency'),
            'user_name' => Yii::t('intellect-money/order', 'label.user_name'),
            'user_email' => Yii::t('intellect-money/order', 'label.user_email'),
            'success_url' => Yii::t('intellect-money/order', 'label.success_url'),
            'preference' => Yii::t('intellect-money/order', 'label.preference'),
            'expire_date' => Yii::t('intellect-money/order', 'label.expire_date'),
            'params' => Yii::t('intellect-money/order', 'label.params'),
            'hash' => Yii::t('intellect-money/order', 'label.hash'),
            'created_at' => Yii::t('intellect-money/order', 'label.created_at'),
            'updated_at' => Yii::t('intellect-money/order', 'label.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('intellect-money/order', 'hint.id'),
            'user_id' => Yii::t('intellect-money/order', 'hint.user_id'),
            'eshop_id' => Yii::t('intellect-money/order', 'hint.eshop_id'),
            'service_name' => Yii::t('intellect-money/order', 'hint.service_name'),
            'sum' => Yii::t('intellect-money/order', 'hint.sum'),
            'recipient_currency' => Yii::t('intellect-money/order', 'hint.recipient_currency'),
            'user_name' => Yii::t('intellect-money/order', 'hint.user_name'),
            'user_email' => Yii::t('intellect-money/order', 'hint.user_email'),
            'success_url' => Yii::t('intellect-money/order', 'hint.success_url'),
            'preference' => Yii::t('intellect-money/order', 'hint.preference'),
            'expire_date' => Yii::t('intellect-money/order', 'hint.expire_date'),
            'params' => Yii::t('intellect-money/order', 'hint.params'),
            'hash' => Yii::t('intellect-money/order', 'hint.hash'),
            'created_at' => Yii::t('intellect-money/order', 'hint.created_at'),
            'updated_at' => Yii::t('intellect-money/order', 'hint.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('intellect-money/order', 'placeholder.id'),
            'user_id' => Yii::t('intellect-money/order', 'placeholder.user_id'),
            'eshop_id' => Yii::t('intellect-money/order', 'placeholder.eshop_id'),
            'service_name' => Yii::t('intellect-money/order', 'placeholder.service_name'),
            'sum' => Yii::t('intellect-money/order', 'placeholder.sum'),
            'recipient_currency' => Yii::t('intellect-money/order', 'placeholder.recipient_currency'),
            'user_name' => Yii::t('intellect-money/order', 'placeholder.user_name'),
            'user_email' => Yii::t('intellect-money/order', 'placeholder.user_email'),
            'success_url' => Yii::t('intellect-money/order', 'placeholder.success_url'),
            'preference' => Yii::t('intellect-money/order', 'placeholder.preference'),
            'expire_date' => Yii::t('intellect-money/order', 'placeholder.expire_date'),
            'params' => Yii::t('intellect-money/order', 'placeholder.params'),
            'hash' => Yii::t('intellect-money/order', 'placeholder.hash'),
            'created_at' => Yii::t('intellect-money/order', 'placeholder.created_at'),
            'updated_at' => Yii::t('intellect-money/order', 'placeholder.updated_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from UserId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set value to UserId
     *
     * @param $value
     * @return $this
     */
    public function setUserId($value)
    {
        $this->user_id = $value;
        return $this;
    }

    /**
     * Get value from EshopId
     *
     * @return string
     */
    public function getEshopId()
    {
        return $this->eshop_id;
    }

    /**
     * Set value to EshopId
     *
     * @param $value
     * @return $this
     */
    public function setEshopId($value)
    {
        $this->eshop_id = $value;
        return $this;
    }

    /**
     * Get value from ServiceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->service_name;
    }

    /**
     * Set value to ServiceName
     *
     * @param $value
     * @return $this
     */
    public function setServiceName($value)
    {
        $this->service_name = $value;
        return $this;
    }

    /**
     * Get value from Sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set value to Sum
     *
     * @param $value
     * @return $this
     */
    public function setSum($value)
    {
        $this->sum = $value;
        return $this;
    }

    /**
     * Get value from RecipientCurrency
     *
     * @return string
     */
    public function getRecipientCurrency()
    {
        return $this->recipient_currency;
    }

    /**
     * Set value to RecipientCurrency
     *
     * @param $value
     * @return $this
     */
    public function setRecipientCurrency($value)
    {
        $this->recipient_currency = $value;
        return $this;
    }

    /**
     * Get value from UserName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set value to UserName
     *
     * @param $value
     * @return $this
     */
    public function setUserName($value)
    {
        $this->user_name = $value;
        return $this;
    }

    /**
     * Get value from UserEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * Set value to UserEmail
     *
     * @param $value
     * @return $this
     */
    public function setUserEmail($value)
    {
        $this->user_email = $value;
        return $this;
    }

    /**
     * Get value from SuccessUrl
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->success_url;
    }

    /**
     * Set value to SuccessUrl
     *
     * @param $value
     * @return $this
     */
    public function setSuccessUrl($value)
    {
        $this->success_url = $value;
        return $this;
    }

    /**
     * Get value from Preference
     *
     * @return string
     */
    public function getPreference()
    {
        return $this->preference;
    }

    /**
     * Set value to Preference
     *
     * @param $value
     * @return $this
     */
    public function setPreference($value)
    {
        $this->preference = $value;
        return $this;
    }

    /**
     * Get value from ExpireDate
     *
     * @return string
     */
    public function getExpireDate()
    {
        return $this->expire_date;
    }

    /**
     * Set value to ExpireDate
     *
     * @param $value
     * @return $this
     */
    public function setExpireDate($value)
    {
        $this->expire_date = $value;
        return $this;
    }

    /**
     * Get value from Params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set value to Params
     *
     * @param $value
     * @return $this
     */
    public function setParams($value)
    {
        $this->params = $value;
        return $this;
    }

    /**
     * Get value from Hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set value to Hash
     *
     * @param $value
     * @return $this
     */
    public function setHash($value)
    {
        $this->hash = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * Get relation Payment
     *
     * @param string $class
     * @return query\PaymentActiveQuery
     */
    public function getPayment($class = '\Payment')
    {
        return $this->hasOne(static::findClass($class, __NAMESPACE__), ['order_id' => 'id']);
    }

}
