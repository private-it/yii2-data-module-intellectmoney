<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\intellectmoney\models;

use PrivateIT\modules\intellectmoney\IntellectMoneyModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Payment
 *
 * @property integer $id
 * @property integer $eshop_id
 * @property integer $payment_id
 * @property integer $order_id
 * @property string $eshop_account
 * @property string $service_name
 * @property string $recipient_original_amount
 * @property string $recipient_amount
 * @property string $recipient_currency
 * @property integer $payment_status
 * @property string $user_name
 * @property string $user_email
 * @property string $payment_data
 * @property string $pay_method
 * @property string $short_pan
 * @property string $country
 * @property string $bank
 * @property string $ip_address
 * @property string $secret_key
 * @property string $hash
 * @property string $params
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order[] $order
 */
class Payment extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('intellect-money/payment', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('intellect-money/payment', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('intellect-money/payment', 'const.status.active'),
        ];
    }

    /**
     * @inheritdoc
     * @return query\PaymentActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return IntellectMoneyModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('intellect-money/payment', 'label.id'),
            'eshop_id' => Yii::t('intellect-money/payment', 'label.eshop_id'),
            'payment_id' => Yii::t('intellect-money/payment', 'label.payment_id'),
            'order_id' => Yii::t('intellect-money/payment', 'label.order_id'),
            'eshop_account' => Yii::t('intellect-money/payment', 'label.eshop_account'),
            'service_name' => Yii::t('intellect-money/payment', 'label.service_name'),
            'recipient_original_amount' => Yii::t('intellect-money/payment', 'label.recipient_original_amount'),
            'recipient_amount' => Yii::t('intellect-money/payment', 'label.recipient_amount'),
            'recipient_currency' => Yii::t('intellect-money/payment', 'label.recipient_currency'),
            'payment_status' => Yii::t('intellect-money/payment', 'label.payment_status'),
            'user_name' => Yii::t('intellect-money/payment', 'label.user_name'),
            'user_email' => Yii::t('intellect-money/payment', 'label.user_email'),
            'payment_data' => Yii::t('intellect-money/payment', 'label.payment_data'),
            'pay_method' => Yii::t('intellect-money/payment', 'label.pay_method'),
            'short_pan' => Yii::t('intellect-money/payment', 'label.short_pan'),
            'country' => Yii::t('intellect-money/payment', 'label.country'),
            'bank' => Yii::t('intellect-money/payment', 'label.bank'),
            'ip_address' => Yii::t('intellect-money/payment', 'label.ip_address'),
            'secret_key' => Yii::t('intellect-money/payment', 'label.secret_key'),
            'hash' => Yii::t('intellect-money/payment', 'label.hash'),
            'params' => Yii::t('intellect-money/payment', 'label.params'),
            'created_at' => Yii::t('intellect-money/payment', 'label.created_at'),
            'updated_at' => Yii::t('intellect-money/payment', 'label.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('intellect-money/payment', 'hint.id'),
            'eshop_id' => Yii::t('intellect-money/payment', 'hint.eshop_id'),
            'payment_id' => Yii::t('intellect-money/payment', 'hint.payment_id'),
            'order_id' => Yii::t('intellect-money/payment', 'hint.order_id'),
            'eshop_account' => Yii::t('intellect-money/payment', 'hint.eshop_account'),
            'service_name' => Yii::t('intellect-money/payment', 'hint.service_name'),
            'recipient_original_amount' => Yii::t('intellect-money/payment', 'hint.recipient_original_amount'),
            'recipient_amount' => Yii::t('intellect-money/payment', 'hint.recipient_amount'),
            'recipient_currency' => Yii::t('intellect-money/payment', 'hint.recipient_currency'),
            'payment_status' => Yii::t('intellect-money/payment', 'hint.payment_status'),
            'user_name' => Yii::t('intellect-money/payment', 'hint.user_name'),
            'user_email' => Yii::t('intellect-money/payment', 'hint.user_email'),
            'payment_data' => Yii::t('intellect-money/payment', 'hint.payment_data'),
            'pay_method' => Yii::t('intellect-money/payment', 'hint.pay_method'),
            'short_pan' => Yii::t('intellect-money/payment', 'hint.short_pan'),
            'country' => Yii::t('intellect-money/payment', 'hint.country'),
            'bank' => Yii::t('intellect-money/payment', 'hint.bank'),
            'ip_address' => Yii::t('intellect-money/payment', 'hint.ip_address'),
            'secret_key' => Yii::t('intellect-money/payment', 'hint.secret_key'),
            'hash' => Yii::t('intellect-money/payment', 'hint.hash'),
            'params' => Yii::t('intellect-money/payment', 'hint.params'),
            'created_at' => Yii::t('intellect-money/payment', 'hint.created_at'),
            'updated_at' => Yii::t('intellect-money/payment', 'hint.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('intellect-money/payment', 'placeholder.id'),
            'eshop_id' => Yii::t('intellect-money/payment', 'placeholder.eshop_id'),
            'payment_id' => Yii::t('intellect-money/payment', 'placeholder.payment_id'),
            'order_id' => Yii::t('intellect-money/payment', 'placeholder.order_id'),
            'eshop_account' => Yii::t('intellect-money/payment', 'placeholder.eshop_account'),
            'service_name' => Yii::t('intellect-money/payment', 'placeholder.service_name'),
            'recipient_original_amount' => Yii::t('intellect-money/payment', 'placeholder.recipient_original_amount'),
            'recipient_amount' => Yii::t('intellect-money/payment', 'placeholder.recipient_amount'),
            'recipient_currency' => Yii::t('intellect-money/payment', 'placeholder.recipient_currency'),
            'payment_status' => Yii::t('intellect-money/payment', 'placeholder.payment_status'),
            'user_name' => Yii::t('intellect-money/payment', 'placeholder.user_name'),
            'user_email' => Yii::t('intellect-money/payment', 'placeholder.user_email'),
            'payment_data' => Yii::t('intellect-money/payment', 'placeholder.payment_data'),
            'pay_method' => Yii::t('intellect-money/payment', 'placeholder.pay_method'),
            'short_pan' => Yii::t('intellect-money/payment', 'placeholder.short_pan'),
            'country' => Yii::t('intellect-money/payment', 'placeholder.country'),
            'bank' => Yii::t('intellect-money/payment', 'placeholder.bank'),
            'ip_address' => Yii::t('intellect-money/payment', 'placeholder.ip_address'),
            'secret_key' => Yii::t('intellect-money/payment', 'placeholder.secret_key'),
            'hash' => Yii::t('intellect-money/payment', 'placeholder.hash'),
            'params' => Yii::t('intellect-money/payment', 'placeholder.params'),
            'created_at' => Yii::t('intellect-money/payment', 'placeholder.created_at'),
            'updated_at' => Yii::t('intellect-money/payment', 'placeholder.updated_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from EshopId
     *
     * @return string
     */
    public function getEshopId()
    {
        return $this->eshop_id;
    }

    /**
     * Set value to EshopId
     *
     * @param $value
     * @return $this
     */
    public function setEshopId($value)
    {
        $this->eshop_id = $value;
        return $this;
    }

    /**
     * Get value from PaymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->payment_id;
    }

    /**
     * Set value to PaymentId
     *
     * @param $value
     * @return $this
     */
    public function setPaymentId($value)
    {
        $this->payment_id = $value;
        return $this;
    }

    /**
     * Get value from OrderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set value to OrderId
     *
     * @param $value
     * @return $this
     */
    public function setOrderId($value)
    {
        $this->order_id = $value;
        return $this;
    }

    /**
     * Get value from EshopAccount
     *
     * @return string
     */
    public function getEshopAccount()
    {
        return $this->eshop_account;
    }

    /**
     * Set value to EshopAccount
     *
     * @param $value
     * @return $this
     */
    public function setEshopAccount($value)
    {
        $this->eshop_account = $value;
        return $this;
    }

    /**
     * Get value from ServiceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->service_name;
    }

    /**
     * Set value to ServiceName
     *
     * @param $value
     * @return $this
     */
    public function setServiceName($value)
    {
        $this->service_name = $value;
        return $this;
    }

    /**
     * Get value from RecipientOriginalAmount
     *
     * @return string
     */
    public function getRecipientOriginalAmount()
    {
        return $this->recipient_original_amount;
    }

    /**
     * Set value to RecipientOriginalAmount
     *
     * @param $value
     * @return $this
     */
    public function setRecipientOriginalAmount($value)
    {
        $this->recipient_original_amount = $value;
        return $this;
    }

    /**
     * Get value from RecipientAmount
     *
     * @return string
     */
    public function getRecipientAmount()
    {
        return $this->recipient_amount;
    }

    /**
     * Set value to RecipientAmount
     *
     * @param $value
     * @return $this
     */
    public function setRecipientAmount($value)
    {
        $this->recipient_amount = $value;
        return $this;
    }

    /**
     * Get value from RecipientCurrency
     *
     * @return string
     */
    public function getRecipientCurrency()
    {
        return $this->recipient_currency;
    }

    /**
     * Set value to RecipientCurrency
     *
     * @param $value
     * @return $this
     */
    public function setRecipientCurrency($value)
    {
        $this->recipient_currency = $value;
        return $this;
    }

    /**
     * Get value from PaymentStatus
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * Set value to PaymentStatus
     *
     * @param $value
     * @return $this
     */
    public function setPaymentStatus($value)
    {
        $this->payment_status = $value;
        return $this;
    }

    /**
     * Get value from UserName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set value to UserName
     *
     * @param $value
     * @return $this
     */
    public function setUserName($value)
    {
        $this->user_name = $value;
        return $this;
    }

    /**
     * Get value from UserEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * Set value to UserEmail
     *
     * @param $value
     * @return $this
     */
    public function setUserEmail($value)
    {
        $this->user_email = $value;
        return $this;
    }

    /**
     * Get value from PaymentData
     *
     * @return string
     */
    public function getPaymentData()
    {
        return $this->payment_data;
    }

    /**
     * Set value to PaymentData
     *
     * @param $value
     * @return $this
     */
    public function setPaymentData($value)
    {
        $this->payment_data = $value;
        return $this;
    }

    /**
     * Get value from PayMethod
     *
     * @return string
     */
    public function getPayMethod()
    {
        return $this->pay_method;
    }

    /**
     * Set value to PayMethod
     *
     * @param $value
     * @return $this
     */
    public function setPayMethod($value)
    {
        $this->pay_method = $value;
        return $this;
    }

    /**
     * Get value from ShortPan
     *
     * @return string
     */
    public function getShortPan()
    {
        return $this->short_pan;
    }

    /**
     * Set value to ShortPan
     *
     * @param $value
     * @return $this
     */
    public function setShortPan($value)
    {
        $this->short_pan = $value;
        return $this;
    }

    /**
     * Get value from Country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set value to Country
     *
     * @param $value
     * @return $this
     */
    public function setCountry($value)
    {
        $this->country = $value;
        return $this;
    }

    /**
     * Get value from Bank
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set value to Bank
     *
     * @param $value
     * @return $this
     */
    public function setBank($value)
    {
        $this->bank = $value;
        return $this;
    }

    /**
     * Get value from IpAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Set value to IpAddress
     *
     * @param $value
     * @return $this
     */
    public function setIpAddress($value)
    {
        $this->ip_address = $value;
        return $this;
    }

    /**
     * Get value from SecretKey
     *
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secret_key;
    }

    /**
     * Set value to SecretKey
     *
     * @param $value
     * @return $this
     */
    public function setSecretKey($value)
    {
        $this->secret_key = $value;
        return $this;
    }

    /**
     * Get value from Hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set value to Hash
     *
     * @param $value
     * @return $this
     */
    public function setHash($value)
    {
        $this->hash = $value;
        return $this;
    }

    /**
     * Get value from Params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set value to Params
     *
     * @param $value
     * @return $this
     */
    public function setParams($value)
    {
        $this->params = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * Get relation Order[]
     *
     * @param string $class
     * @return query\OrderActiveQuery
     */
    public function getOrder($class = '\Order')
    {
        return $this->hasMany(static::findClass($class, __NAMESPACE__), ['id' => 'order_id']);
    }

}
