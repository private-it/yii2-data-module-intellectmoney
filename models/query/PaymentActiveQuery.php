<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\intellectmoney\models\query;

use PrivateIT\modules\intellectmoney\models\Payment;

/**
 * PaymentActiveQuery
 *
 */
class PaymentActiveQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Payment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Payment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */
}
